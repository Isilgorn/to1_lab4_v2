import java.util.EnumSet;

public class Student {
    private String firstName;
    private String lastName;
    private AdditionalRole role;
    private Grades grades;

    public Student(String firstName, String lastName, AdditionalRole role, Grades grades) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.grades = grades;
        this.role = role;
    }

    public void addGrade(Grade grade) { this.grades.addGrade(grade);}

    public void delGrade(Grade grade) { this.grades.delGrade(grade);}

    public double getAVG() {return this.grades.getAVG();}

    public boolean setRole(AdditionalRole role) {
        this.role = role;
        return true;
    }

    public boolean removeRole(AdditionalRole role) {
        this.role = null;
        return true;
    }

    public AdditionalRole getRoles() {
        return role;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }
}
