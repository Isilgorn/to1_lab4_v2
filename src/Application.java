public class Application {
    public static void main(String[] args) {
        Department[] deps = new Department[]{new Department("WIET"), new Department("EAIB")};
        FieldOfStudy[] fields = new FieldOfStudy[]{new FieldOfStudy("Informatyka"), new FieldOfStudy("Elektronika")};
        University universe = new University();
        for(int i = 0; i < 2; ++ i){
            universe.addDepartment(deps[i]);
            for(int j = 0; j < 2; ++j)
                universe.addFieldOfStudy(fields[i], deps[i]);
        }

        Student[] students = new Student[]{new Student("jsn", "kow", AdditionalRole.GROUP_REPRESENTATIVE, new Grades()), new Student("rt", "as", null, new Grades())};
        students[0].addGrade(Grade.BDB);
        students[0].addGrade(Grade.DST);
        students[1].addGrade(Grade.DB_PLUS);
        universe.addStudent(students[0], fields[0], deps[0]);
        universe.addStudent(students[1], fields[1], deps[1]);

        universe.showStudents();
    }
}
