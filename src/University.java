import java.util.*;

public class University {
    private Map<Department, Map<FieldOfStudy, List<Student>>> students = new HashMap<Department, Map<FieldOfStudy, List<Student>>>();
    private double[] gradeLim = new double[]{4.5, 4.0};
    private int[] money = new int[]{1000, 500};

    public void addDepartment(Department department){
        students.put(department, new HashMap<FieldOfStudy, List<Student>>());
    }

    public void addFieldOfStudy(FieldOfStudy fieldOfStudy, Department department){
        students.get(department).put(fieldOfStudy, new LinkedList<Student>());
    }

    public void addStudent(Student student, FieldOfStudy fieldOfStudy, Department department){
        students.get(department).get(fieldOfStudy).add(student);
    }

    public void showStudents(){
        for(Department d : students.keySet()){
            Map<FieldOfStudy, List<Student>> department = students.get(d);
            for(FieldOfStudy f : department.keySet()){
                List<Student> fieldOfStudy = department.get(f);
                for(Student s : fieldOfStudy) {
                    double avg = s.getAVG();
                    int scholarship = 0;
                    for(int i = 0; i < gradeLim.length; ++i)
                        if(avg > gradeLim[i]){
                            scholarship += money[i];
                            break;
                        }
                    AdditionalRole tmp = s.getRoles();
                    String roles = "";
                    if(tmp != null){
                        scholarship += tmp.getScholarship();
                        roles += ", " + tmp;
                    }
                    System.out.println(s.getFirstName() + " " + s.getLastName() + ", " + d + ", " + f + ", średnia ocen "
                    + avg + roles + ", stypendium " + scholarship);
                }
            }
        }
    }
}
