public class Department {
    private String department;

    public Department(String department){
        this.department = department;
    }

    @Override
    public String toString(){
        return "wydział " + department;
    }
}
