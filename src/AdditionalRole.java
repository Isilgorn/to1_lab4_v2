public enum AdditionalRole {
    GROUP_REPRESENTATIVE(50, "starosta grupy"),
    YEAR_REPRESENTATIVE(100, "starosta roku"),
    STUDENT_COUNCIL_MEMBER(150, "członek samorządu studentów");

    private int scholarship; // int ??
    private String description;

    AdditionalRole(int scholarship, String description) {
        this.scholarship = scholarship;
        this.description = description;
    }

    public int getScholarship() {
        return scholarship;
    }

    @Override
    public String toString() {
        return description;
    }
}
