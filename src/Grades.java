import java.util.LinkedList;
import java.util.List;

public class Grades {
    private List<Grade> grades = new LinkedList<>();

    public void addGrade(Grade grade){
        grades.add(grade);
    }

    public void delGrade(Grade grade){
        grades.remove(grade);
    }

    public double getAVG(){
        double sum = 0;
        for(Grade grade : grades)
            sum += grade.getGrade();
        return sum / grades.size();
    }
}
