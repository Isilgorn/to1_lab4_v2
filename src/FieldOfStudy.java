public class FieldOfStudy {
    private String fieldOfStudy;

    public FieldOfStudy(String fieldOfStudy){
        this.fieldOfStudy = fieldOfStudy;
    }

    @Override
    public String toString(){
        return "kierunek " + fieldOfStudy;
    }
}
