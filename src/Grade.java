import java.util.Collection;

public enum Grade{
    BDB(5.0),
    DB_PLUS(4.5),
    DB(4.0),
    DST_PLUS(3.5),
    DST(3.0),
    NDST(2.0);

    private double grade; // double ??

    Grade(double grade) {
        this.grade = grade;
    }

    public double getGrade() {
        return grade;
    }
}
